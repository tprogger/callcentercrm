<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpClient\NativeHttpClient;
use phpseclib\Net\SSH2;

use App\Entity\User;
ini_set('memory_limit', '-1');

class DashboardClosersController extends AbstractController
{
	private $security;
	private $client;

    public function __construct(Security $security, HttpClientInterface $client)
    {
		$this->security = $security;
		$this->client = $client;
    }

    /**
     * @Route("/dashboard/superadmin/closers", name="dashboard_closers")
     */
    public function index(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {

		if(!empty($_POST)){
			if(!empty($_POST['closer_id']) && !empty($_POST['closer_sum'])){
				$entityManager = $this->getDoctrine()->getManager();
				$users_rep = $this->getDoctrine()->getRepository(User::class);
				$topup_user = $users_rep->findOneBy([
					'id' => $_POST['closer_id']
				]);
				$old_balance = $topup_user->getBalance();
				$new_balance = $_POST['closer_sum']+$old_balance;
				$topup_user->setBalance($new_balance);
				$entityManager->persist($topup_user);
				$entityManager->flush();
			}elseif(!empty($_POST['closer_login']) && !empty($_POST['closer_password']) && !empty($_POST['closer_sip_id']) && !empty($_POST['closer_sip_pwd'])){
				$role = ['ROLE_CLOSER'];
				$closer = new User();
				$closer->setUsername($_POST['closer_login']);
				$closer->setPassword(
					$passwordEncoder->encodePassword(
						$closer,
						$_POST['closer_password']
					)
				);
				$closer->setRoles($role);
				$closer->setStatus(0);
				$closer->setFullname($_POST['closer_sip_id']);
				$closer->setAccessRobot(0);
				$closer->setCloserId(0);
				$closer->setSipId($_POST['closer_sip_id']);
				$closer->setSipPassword($_POST['closer_sip_pwd']);
				$closer->setBalance(0);
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($closer);
				$entityManager->flush();
				$sip_id = $_POST['closer_sip_id'];
				$sip_pass = $_POST['closer_sip_pwd'];

				$client = new CurlHttpClient();

				$getAccessToken = $client->request('POST', $_ENV["AMPORTAL_URL"].'/authenticate', [
					'body' => 'user='.$_ENV["AMPORTAL_ADMIN_LOGIN"].'&password='.$_ENV["AMPORTAL_ADMIN_PASSWORD"]
				]);
				$payload = (object)$getAccessToken->toArray();
				$access_token = $payload->access_token;

				$header = 'Bearer '.$access_token;

				$getAllExtensions = $client->request('GET', $_ENV["AMPORTAL_URL"].'/extensions', [
					'headers' => [
						'Authorization' => $header,
					]
				])->getContent();
				$jsonExtensions = (object)json_decode($getAllExtensions, TRUE);
				$all_extensions = $jsonExtensions->results;

				$user_data = array(
					'name' => $_POST['closer_sip_id'],
					'secret' => $_POST['closer_sip_pwd'],
				);

				$create_url = $_ENV["AMPORTAL_URL"]."/extensions/".$_POST['closer_sip_id'];
				$create = $client->request('PUT', $create_url, [
					'headers' => [
						'Authorization' => $header,
					],
					'body' => json_encode($user_data)
				]);
			}
			return $this->redirectToRoute('dashboard_closers');
		}

    	$user = $this->getUser();
		$user_role = $user->getRoles();
		$users_rep = $this->getDoctrine()->getRepository(User::class);
    	$closers = $users_rep->findByRole('ROLE_CLOSER');

        return $this->render('default/closers.html.twig', [
            'closers' => $closers,
        ]);
	}


	/**
     * @Route("/dashboard/superadmin/closers/{cid}/manager", name="dashboard_closers_manager")
     */
    public function manager($cid, Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
		function randomPassword() {
			$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
			$pass = array();
			$alphaLength = strlen($alphabet) - 1;
			for ($i = 0; $i < 16; $i++) {
				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];
			}
			return implode($pass).rand(11,999);
		}


		$entityManager = $this->getDoctrine()->getManager();
		if(!empty($_POST)){
			if(!empty($_POST['new_closer_password'])){
				$users_rep = $this->getDoctrine()->getRepository(User::class);
				$closer = $users_rep->findOneBy([
					'id' => $cid
				]);
				$closer->setPassword(
					$passwordEncoder->encodePassword(
						$closer,
						$_POST['closer_password']
					)
				);
				$entityManager->persist($closer);
				$entityManager->flush();
				return $this->redirectToRoute('dashboard_closers_manager',[
					'cid' => $cid
				]);
			}

			if(!empty($_POST['new_users_to_closer']) && !empty($_POST['new_users_to_closer_prefix'])){
				$client = new CurlHttpClient();

				$getAccessToken = $client->request('POST', $_ENV["AMPORTAL_URL"].'/authenticate', [
					'body' => 'user='.$_ENV["AMPORTAL_ADMIN_LOGIN"].'&password='.$_ENV["AMPORTAL_ADMIN_PASSWORD"]
				]);
				$payload = (object)$getAccessToken->toArray();
				$access_token = $payload->access_token;

				$header = 'Bearer '.$access_token;

				$start_number = $_POST['new_users_to_closer_prefix'];
				$last_number = $_POST['new_users_to_closer_prefix']+$_POST['new_users_to_closer'];
				for ($i=$start_number; $i < $last_number; $i++) { 
					$role = ['ROLE_USER'];
					$usr_password = randomPassword();

					$usr = new User();
					$usr->setUsername($i);
					$usr->setPassword(
						$passwordEncoder->encodePassword(
							$usr,
							$usr_password
						)
					);
					$usr->setRoles($role);
					$usr->setStatus(0);
					$usr->setFullname($i);
					$usr->setAccessRobot(0);
					$usr->setCloserId($cid);
					$usr->setSipId($i);
					$usr->setSipPassword($usr_password);
					$usr->setBalance(0);
					$entityManager = $this->getDoctrine()->getManager();
					$entityManager->persist($usr);
					$entityManager->flush();

					$user_data = array(
						'name' => $i,
						'secret' => $usr_password,
					);
	
					$create_url = $_ENV["AMPORTAL_URL"]."/extensions/".$i;
					$create = $client->request('PUT', $create_url, [
						'headers' => [
							'Authorization' => $header,
						],
						'body' => json_encode($user_data)
					]);
				}
				return $this->redirectToRoute('dashboard_closers_manager',[
					'cid' => $cid
				]);
			}

		}

    	$user = $this->getUser();
		$user_role = $user->getRoles();
		$users_rep = $this->getDoctrine()->getRepository(User::class);
    	$operators = $users_rep->findBy([
			'closer_id' => $cid
		]);

		$optext = '';
		foreach ($operators as $key => $value) {
			$optext .= $value->getSipId().':'.$value->getSipPassword().PHP_EOL;
		}

        return $this->render('default/closers_manager.html.twig', [
			'operators' => $operators,
			'optext' => $optext
        ]);
	}

	/**
     * @Route("/dashboard/superadmin/closers/{cid}/manager/{uid}/remove", name="dashboard_closers_user_remove")
     */
    public function remove_operator($cid, $uid)
    {
		$client = new CurlHttpClient();

		$getAccessToken = $client->request('POST', $_ENV["AMPORTAL_URL"].'/authenticate', [
			'body' => 'user='.$_ENV["AMPORTAL_ADMIN_LOGIN"].'&password='.$_ENV["AMPORTAL_ADMIN_PASSWORD"]
		]);
		$payload = (object)$getAccessToken->toArray();
		$access_token = $payload->access_token;

		$header = 'Bearer '.$access_token;

		$entityManager = $this->getDoctrine()->getManager();
		$users_rep = $this->getDoctrine()->getRepository(User::class);
    	$usr = $users_rep->findOneBy([
			'closer_id' => $cid,
			'id' => $uid
		]);
		

		$delete_url = $_ENV["AMPORTAL_URL"]."/extensions/".$usr->getSipId();
		$delete = $client->request('DELETE', $delete_url, [
			'headers' => [
				'Authorization' => $header,
			]
		]);

		$entityManager->remove($usr);
		$entityManager->flush();

        return $this->redirectToRoute('dashboard_closers_manager',[
			'cid' => $cid
		]);
	}


	/**
     * @Route("/dashboard/superadmin/closers/{cid}/remove", name="dashboard_closers_remove")
     */
    public function remove($cid)
    {
		$entityManager = $this->getDoctrine()->getManager();
		$users_rep = $this->getDoctrine()->getRepository(User::class);
    	$users = $users_rep->findBy([
			'closer_id' => $cid
		]);

		$client = new CurlHttpClient();

		$getAccessToken = $client->request('POST', $_ENV["AMPORTAL_URL"].'/authenticate', [
			'body' => 'user='.$_ENV["AMPORTAL_ADMIN_LOGIN"].'&password='.$_ENV["AMPORTAL_ADMIN_PASSWORD"]
		]);
		$payload = (object)$getAccessToken->toArray();
		$access_token = $payload->access_token;

		$header = 'Bearer '.$access_token;

		foreach ($users as $key => $value) {
			$delete_url = $_ENV["AMPORTAL_URL"]."/extensions/".$value->getSipId();
			$delete = $client->request('DELETE', $delete_url, [
				'headers' => [
					'Authorization' => $header,
				]
			]);
			$entityManager->remove($value);
			$entityManager->flush();
		}

		$closer = $users_rep->findOneBy([
			'id' => $cid
		]);

		$entityManager->remove($closer);
		$entityManager->flush();

        return $this->redirectToRoute('dashboard_closers');
	}
}
