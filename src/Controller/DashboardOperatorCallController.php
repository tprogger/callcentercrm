<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use App\Entity\Contacts;
use App\Entity\Comments;
use App\Entity\User;

class DashboardOperatorCallController extends AbstractController
{
	private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/operator/call", name="dashboard_operator_call")
     */
    public function index()
    {
    	$user = $this->getUser();
    	$user_id = $user->getId();
        $now_time = new \DateTime("now");
    	$entityManager = $this->getDoctrine()->getManager();
        $clients = $entityManager->getRepository(Contacts::class)->findOneBy([
            'closer_id' => $user->getCloserId(),
            'worker_id' => $user_id,
            'CallType' => 1,
            'call_status' => 0,
        ]);
        if(is_null($clients)){
        	$clients = $entityManager->getRepository(Contacts::class)->findOneBy([
	            'closer_id' => $user->getCloserId(),
	            'worker_id' => 0,
	            'CallType' => 1,
	            'call_status' => 0,
	        ]);
            if(is_null($clients)){
                $fullname = "ФИО";
                $country = "Россия";
                $city = "Москва";
                $region = "Московская область";
                $phone = 79999999999;
                $timezone = 4;
                $date = new \DateTime("now");
                $date->add(new \DateInterval("PT{$timezone}H"));
                $new_time = $date->format('H:i:s');
                $clid = -1;
            }else{
                $fullname = $clients->getFullname();
                $country = $clients->getCountry();
                $city = $clients->getCity();
                $region = $clients->getRegion();
                $phone = $clients->getPhone();
                $timezone = $clients->getTimezone();
                $date = new \DateTime("now");
                $date->add(new \DateInterval("PT{$timezone}H"));
                $new_time = $date->format('H:i:s');
                $clid = $clients->getId();
                $clients->setWorkerId($user_id);
                $entityManager->persist($clients);
                $entityManager->flush();
            }
        	
        }else{
        	$clients = $entityManager->getRepository(Contacts::class)->findOneBy([
	            'closer_id' => $user->getCloserId(),
	            'worker_id' => $user->getId(),
	            'CallType' => 1,
	            'call_status' => 0,
	        ]);
            if(is_null($clients)){
                $fullname = "ФИО";
                $country = "Россия";
                $city = "Москва";
                $region = "Московская область";
                $phone = 79999999999;
                $timezone = 4;
                $date = new \DateTime("now");
                $date->add(new \DateInterval("PT{$timezone}H"));
                $new_time = $date->format('H:i:s');
                $clid = -1;
            }else{
                $fullname = $clients->getFullname();
                $country = $clients->getCountry();
                $city = $clients->getCity();
                $region = $clients->getRegion();
                $phone = $clients->getPhone();
                $timezone = $clients->getTimezone();
                $date = new \DateTime("now");
                $date->add(new \DateInterval("PT{$timezone}H"));
                $new_time = $date->format('H:i:s');
                $clid = $clients->getId();

            }
        }

        // Обрабатываем комментарии
        $comments = [];

        if($clid != -1){
            $comments = $entityManager->getRepository(Comments::class)->findBy([
                'contact_id' => $clid,
            ], ['id' => 'DESC']);
            foreach ($comments as $key => $value) {
                $uid = $value->getUserId();
                if($uid == -1){
                    $value->user_name = "Система";
                }else{
                    $user_name = $entityManager->getRepository(User::class)->findOneBy([
                        'id' => $uid
                    ])->getSipId();
                    $value->user_name = $user_name;    
                }
            }
        }

        
        return $this->render('default/operator_call.html.twig', [
            'fullname' => $fullname,
            'country' => $country,
            'city' => $city,
            'region' => $region,
            'phone' => $phone,
            'localtime' => $new_time,
            'clid' => $clid,
            'comments' => $comments,
            'nowtime' => $now_time,
            'user' => $user
        ]);
    }

    /**
     * @Route("/operator/call/{id}/setstatus/{sid}", name="dashboard_setstatus")
     */
    public function setstatus($id, $sid)
    {
        if($id != -1){
            $user = $this->getUser();
            $user_id = $user->getId();

            $entityManager = $this->getDoctrine()->getManager();

            $contact = $entityManager->getRepository(Contacts::class)->findOneBy(['id' => $id]);
            
            $is_admin = in_array(strtoupper('ROLE_ADMIN'), $user->getRoles(), true);
            $is_closer = in_array(strtoupper('ROLE_ADMIN'), $user->getRoles(), true);

            if($contact->getWorkerId() == $user_id || $is_admin == true || $is_closer == true){
                //Меняем статус звонка
                $contact->setCallStatus($sid);
                $entityManager->persist($contact);
                $entityManager->flush(); 

                // Время
                $now_time = new \DateTime();

                //Статусы
                if($sid == 5){
                    $status_text = 'Отказ';
                }elseif($sid == 4){
                    $status_text = 'Помещено в очередь';
                }elseif($sid == 3){
                    $status_text = 'Автоответчик';
                }elseif($sid == 2){
                    $status_text = 'Не удалось дозвонится';
                }elseif($sid == 1){
                    $status_text = 'Успешная сделка';
                }else{
                    $status_text = 'Ожидает оператора';
                }

                // Коммент
                $comment_text = 'SIP ID '.$user->getSipId().' сменил статус контакта "'.$contact->getFullName().'" на "'.$status_text.'"';

                // Оставляем коммент
                $comment = new Comments();
                $comment->setText($comment_text);
                $comment->setDate($now_time);
                $comment->setUserId(-1);
                $comment->setContactId($id);
                $comment->setType(2);
                $entityManager->persist($comment);
                $entityManager->flush(); 
            }  
        }
        return $this->redirectToRoute('dashboard_operator_call');
    }

    /**
     * @Route("/operator/call/{id}/sendcomment", name="dashboard_sendcomment")
     */
    public function sendcomment($id)
    {
        if($id != -1){
            $user = $this->getUser();
            $user_id = $user->getId();

            $is_admin = in_array(strtoupper('ROLE_ADMIN'), $user->getRoles(), true);
            $is_closer = in_array(strtoupper('ROLE_ADMIN'), $user->getRoles(), true);

            $entityManager = $this->getDoctrine()->getManager();

            $contact = $entityManager->getRepository(Contacts::class)->findOneBy(['id' => $id]);


            if($contact->getWorkerId() == $user_id || $is_admin == true || $is_closer == true){
                // Время
                $now_time = new \DateTime();

                // Коммент
                $data = (object) $_POST;
                $comment_text = $data->comment_text;

                // Оставляем коммент
                $comment = new Comments();
                $comment->setText($comment_text);
                $comment->setDate($now_time);
                $comment->setUserId($user_id);
                $comment->setContactId($id);
                $comment->setType(1);
                $entityManager->persist($comment);
            }
            $entityManager->flush();
            $srv = (object) $_SERVER;
            $referer = $srv->HTTP_REFERER;
        }
        if(!empty($referer)){
            return $this->redirect($referer);    
        }else{
            return $this->redirectToRoute('dashboard_operator_call');    
        }
    }
}
