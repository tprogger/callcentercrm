<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Security\Core\Security;

use App\Entity\User;
use App\Entity\Comments;
use App\Entity\Contacts;
use App\Entity\DatabaseCall;


class DashboardDestroyController extends AbstractController
{
	private $security;

	public function __construct(Security $security)
	{
		$this->security = $security;
	}

    /**
     * @Route("/dashboard/destroy", name="dashboard_destruy")
     */
    public function index(Request $request, SluggerInterface $slugger): Response
    {

    	$user = $this->getUser();
    	$user_id = $user->getId();

    	$is_admin = in_array(strtoupper('ROLE_ADMIN'), $user->getRoles(), true);
        $is_closer = in_array(strtoupper('ROLE_CLOSER'), $user->getRoles(), true);

        $entityManager = $this->getDoctrine()->getManager();

        if($is_admin == true OR $is_closer == true){
        	$users = $entityManager->getRepository(User::class)->findBy([
				'closer_id' => $user_id
			]);
			foreach ($users as $key => $value) {
				$comments = $entityManager->getRepository(Comments::class)->findBy([
					'user_id' => $value->getId()
				]);
				foreach ($comments as $comment) {
					$entityManager->remove($comment);
					$entityManager->flush();
				}
				$entityManager->remove($value);
				$entityManager->flush();
			}

			$databases = $entityManager->getRepository(DatabaseCall::class)->findBy([
				'closer_id' => $user_id
			]);
			foreach ($databases as $db) {
				$filename = str_replace('/uploads/OutBound', '', $db->getFile());
				$filename = $this->getParameter('OutboundUploadDir').$filename;
				$entityManager->remove($db);
				$entityManager->flush();
			}

			$contacts = $entityManager->getRepository(Contacts::class)->findBy([
				'closer_id' => $user_id
			]);
			foreach ($contacts as $key => $value) {
				$entityManager->remove($value);
				$entityManager->flush();
			}
			$entityManager->remove($user);
			$entityManager->flush();
			return $this->redirectToRoute('app_logout');
        }else{
        	return $this->redirectToRoute('app_logout');
        }
    }
}
