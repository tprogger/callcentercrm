<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpClient\NativeHttpClient;

ini_set('memory_limit', '-1');
class DashboardOperatorsStatusController extends AbstractController
{
	private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/dashboard/operators/status", name="dashboard_operator_status")
     */
    public function index()
    {
        $client = new CurlHttpClient();

		// $getAccessToken = $client->request('POST', $_ENV["AMPORTAL_URL"].'/authenticate', [
		// 	'body' => 'user='.$_ENV["AMPORTAL_ADMIN_LOGIN"].'&password='.$_ENV["AMPORTAL_ADMIN_PASSWORD"]
		// ]);
		// $payload = (object)$getAccessToken->toArray();
		// $access_token = $payload->access_token;
		// $header = 'Bearer '.$access_token;
		$success_array = array();
		if(isset($_GET['oids']) && !empty($_GET['oids'])){
			$oids = $_GET['oids'];
			$oids_array = explode(',', $oids);
			foreach ($oids_array as $key => $value) {
				// $getOpStatus = $client->request('GET', $_ENV["AMPORTAL_URL"].'/manager/extensionstate?extension='.$value, [
				// 	'headers' => [
				// 		'Authorization' => $header,
				// 	]
				// ])->getContent();
				// $jsonStatus= (object)json_decode($getOpStatus, TRUE);
				// dump($jsonStatus);
				// if($jsonStatus->StatusText == 'Unavailable'){
				// 	$status = 'Оффлайн';
				// }elseif($jsonStatus->StatusText == 'Idle'){
				// 	$status = 'Подключён';
				// }elseif($jsonStatus->StatusText == 'InUse'){
				// 	$status = 'В разговоре';
				// }else{
				// 	$status = 'Оффлайн';
				// }
				$ext = $value;
				$success_array[$ext] = array();
				// $success_array[$ext]['status'] = $status;
				$success_array[$ext]['status'] = '...';
			}
			$response = new Response(json_encode($success_array));
			$response->headers->set('Content-Type', 'application/json');
    		return $response;
		}else{
			die();
		}
		return true;
    }
}
