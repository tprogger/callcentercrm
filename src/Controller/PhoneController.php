<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use App\Entity\Contacts;
use App\Entity\Comments;
use App\Entity\User;

class PhoneController extends AbstractController
{
	private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/operator/phone", name="dashboard_operator_phone")
     */
    public function index()
    {
    	$user = $this->getUser();
    	$user_id = $user->getId();
        return $this->render('default/phone.html.twig', [
            'user' => $user
        ]);
    }
}
