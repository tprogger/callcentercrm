<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use App\Entity\Contacts;
use App\Entity\Comments;
use App\Entity\User;

class DashboardContactProfileController extends AbstractController
{
	private $security;

	public function __construct(Security $security)
	{
		$this->security = $security;
	}

	/**
	 * @Route("/dashboard/contact/{clid}", name="dashboard_contact_profile")
	 */
	public function index($clid)
	{
		$user = $this->getUser();
		$user_id = $user->getId();

		$now_time = new \DateTime("now");

		
		$entityManager = $this->getDoctrine()->getManager();

		// Получаем клиента
		$clients = $entityManager->getRepository(Contacts::class)->findOneBy([
			'id' => $clid
		]);

		$is_admin = in_array(strtoupper('ROLE_ADMIN'), $user->getRoles(), true);
        $is_closer = in_array(strtoupper('ROLE_ADMIN'), $user->getRoles(), true);

        if($is_closer == true || $is_admin == true){
        	if($is_closer == true){
        		if($clients->getCloserId() != $user_id){
					return $this->redirectToRoute('dashboard_operator_call');
				}
        	}
        }else{
        	return $this->redirectToRoute('dashboard_operator_call');
        }

		

		$fullname = $clients->getFullname();
		$country = $clients->getCountry();
		$city = $clients->getCity();
		$region = $clients->getRegion();
		$phone = $clients->getPhone();
		$timezone = $clients->getTimezone();
		$date = new \DateTime("now");
		$date->add(new \DateInterval("PT{$timezone}H"));
		$new_time = $date->format('H:i:s');

		// Обрабатываем комментарии
		$comments = [];

		if($clid != -1){
			$comments = $entityManager->getRepository(Comments::class)->findBy([
				'contact_id' => $clid,
			], ['id' => 'DESC']);
			foreach ($comments as $key => $value) {
				$uid = $value->getUserId();
				if($uid == -1){
					$value->user_name = "Система";
				}else{
					$user_name = $entityManager->getRepository(User::class)->findOneBy([
						'id' => $uid
					])->getSipId();
					$value->user_name = $user_name;    
				}
			}
		}


		
		return $this->render('default/operator_call.html.twig', [
			'fullname' => $fullname,
			'country' => $country,
			'city' => $city,
			'region' => $region,
			'phone' => $phone,
			'localtime' => $new_time,
			'clid' => $clid,
			'comments' => $comments,
			'nowtime' => $now_time
		]);
	}
}
