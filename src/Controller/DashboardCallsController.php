<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Dotenv\Dotenv;


use App\Entity\User;

class DashboardCallsController extends AbstractController
{
	private $security;
	
	public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/dashboard/calls/{opid}/{clid}", name="dashboard_calls")
     */
    public function index($opid = 0, $clid = 0)
    {
		$dotenv = new Dotenv();
    	$user = $this->getUser();
		$user_role = $user->getRoles();
		$user_id = $user->getId();
    	if($this->security->isGranted('ROLE_ADMIN')){
    		if($opid != 0){
    			$operators = $this->getDoctrine()->getRepository(User::class)->findBy([
    				'id' => $opid
		    	]);
    		}else{
    			if($clid != 0){
    				$operators = $this->getDoctrine()->getRepository(User::class)->findBy([
    					'closer_id' => $clid
					]);
					$user = $this->getDoctrine()->getRepository(User::class)->findOneBy([
						'id' => $clid
					]);
    			}else{
    				$operators = $this->getDoctrine()->getRepository(User::class)->findBy([]);	
    			}
    		}
    	}else{
			if($this->security->isGranted('ROLE_CLOSER')){
				$clid = $user_id;
			}
    		if($opid != 0 ){
    			$operators = $this->getDoctrine()->getRepository(User::class)->findBy([
    				'id' => $opid
		    	]);
    		}else{
    			$operators = $this->getDoctrine()->getRepository(User::class)->findBy([
		    		'closer_id' => $user_id
				]);
    		}
    		
    	}
		$whereclause = '';
		$i = 0;
		$len = count($operators);
    	foreach($operators as $op) {
    		if ($i == 0) {
		       $whereclause .= '`cnum`=\''.$op->getSipId().'\'';
		    }else{
		    	if ($i == $len - 1) {
		    		$whereclause .= ' OR `cnum`=\''.$op->getSipId().'\'';
		    	}else{
		    		$whereclause .= ' OR `cnum`=\''.$op->getSipId().'\'';
		    	}
		    }
		    $i++;
		}
		
		

    	$link = mysqli_connect($_ENV['MY_MYSQL_HOST'], $_ENV['MY_MYSQL_USER'], $_ENV['MY_MYSQL_PASSWORD'], $_ENV['MY_MYSQL_ASTERISK_DB']);

		if (!$link) {
		    echo "Error: Unable to connect to MySQL." . PHP_EOL;
		    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
		    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
		    exit;
		}
		if (isset($_GET['pageno'])) {
		    $pageno = $_GET['pageno'];
		} else {
		    $pageno = 1;
		}
		$no_of_records_per_page = 10;
		$offset = ($pageno-1) * $no_of_records_per_page;

		if(isset($_GET['dst'])){
			$phone = preg_replace('/[^0-9]/i', '', $_GET['dst']);
			$addwheres = ' AND `dst` LIKE "%'.$phone.'"';
			$dstparam = $phone;
		}else{
			$addwheres = '';
			$dstparam = '';
		}

		$queryy = "SELECT SQL_NO_CACHE * FROM `cdr` WHERE (".$whereclause.")".$addwheres." AND (`disposition` LIKE 'ANSWERED') ORDER BY `calldate` DESC LIMIT $offset,$no_of_records_per_page";
		$result = $link->query($queryy);

		// if($user->getUsername() == 'dnepr1' || $user->getUserName() == 'dnepr3' || $user->getUserName() == 'system33'){
			$query_to_calc = "SELECT SQL_NO_CACHE SUM(`duration`), COUNT(`duration`) FROM `cdr` WHERE (DATE(`calldate`) >= CURRENT_DATE()) AND (".$whereclause.") AND (`disposition` LIKE 'ANSWERED') ORDER BY `calldate`";
			$query_a = "SELECT SQL_NO_CACHE SUM(`duration`), COUNT(`duration`) FROM `cdr` WHERE (DATE(`calldate`) >= CURRENT_DATE()) AND (".$whereclause.") AND (`disposition` LIKE 'ANSWERED') ORDER BY `calldate`";
			$query_c = "SELECT SQL_NO_CACHE SUM(`duration`), COUNT(`duration`) FROM `cdr` WHERE (DATE(`calldate`) >= CURRENT_DATE()) AND (".$whereclause.") AND (`disposition` LIKE 'BUSY' OR `disposition` LIKE 'CONGESTION') ORDER BY `calldate`";
			$query_d = "SELECT SQL_NO_CACHE SUM(`duration`), COUNT(`duration`) FROM `cdr` WHERE (DATE(`calldate`) >= CURRENT_DATE()) AND (".$whereclause.") AND (`disposition` LIKE 'NO ANSWER') ORDER BY `calldate`";
			$query_all_withdrawal = "SELECT SQL_NO_CACHE SUM(`duration`), COUNT(`duration`) FROM `cdr` WHERE (".$whereclause.") AND (`disposition` LIKE 'ANSWERED') ORDER BY `calldate`";
		// }else{
		// 	$query_to_calc = "SELECT SUM(`duration`), COUNT(`duration`) FROM `cdr` WHERE (DATE(`calldate`) >= CURDATE()) AND (".$whereclause.") ORDER BY `calldate`";
		// 	$query_a = "SELECT SUM(`duration`), COUNT(`duration`) FROM `cdr` WHERE (DATE(`calldate`) >= CURDATE()) AND (".$whereclause.") AND `disposition` LIKE 'ANSWERED' ORDER BY `calldate`";
		// 	$query_c = "SELECT SUM(`duration`), COUNT(`duration`) FROM `cdr` WHERE (DATE(`calldate`) >= CURDATE()) AND (".$whereclause.") AND (`disposition` LIKE 'BUSY' OR `disposition` LIKE 'CONGESTION') ORDER BY `calldate`";
		// 	$query_d = "SELECT SUM(`duration`), COUNT(`duration`) FROM `cdr` WHERE (DATE(`calldate`) >= CURDATE()) AND (".$whereclause.") AND (`disposition` LIKE 'NO ANSWER') ORDER BY `calldate`";
		// 	$query_all_withdrawal = "SELECT SUM(`duration`), COUNT(`duration`) FROM `cdr` WHERE (".$whereclause.") ORDER BY `calldate`";
		// }
		
		$result_answered = $link->query($query_to_calc)->fetch_assoc();
		$result_a = $link->query($query_a)->fetch_assoc();
		$result_c = $link->query($query_c)->fetch_assoc();
		$result_d = $link->query($query_d)->fetch_assoc();
		$result_all_withdrawal = $link->query($query_all_withdrawal)->fetch_assoc();


		$time = $link->query("SELECT SUM(`duration`) FROM `cdr` WHERE 1");
		$total_pages_sql = "SELECT COUNT(*) FROM cdr";
		$result_pages = mysqli_query($link,$total_pages_sql);
		$total_rows = mysqli_fetch_array($result_pages)[0];
		$total_pages = ceil($total_rows / $no_of_records_per_page);
		function ensure2Digit($number) {
		    if($number < 10) {
		        $number = '0' . $number;
		    }
		    return $number;
		}
		function secondsToTime($ss) {
		    $s = ensure2Digit($ss%60);
		    $m = ensure2Digit(floor(($ss%3600)/60));
		    $h = ensure2Digit(floor(($ss%86400)/3600));
		    $d = ensure2Digit(floor(($ss%2592000)/86400));
		    $M = ensure2Digit(floor(($ss/2592000)/2592000));
		    $Y = ensure2Digit(floor($ss/31536000));

		    return "$d д. $h ч. $m м. $s с.";
		}
		$tariff = 0.065;
		$all_today_calls = $result_answered['COUNT(`duration`)'];
		if($all_today_calls > 500 && !$this->security->isGranted('ROLE_ADMIN') || $clid != 0){
			if($user->getUsername() == 'dnepr1' || $user->getUsername() == 'dnepr3' || $user->getUsername() == 'dnepr2' || $user->getUsername() == 'dnepr4'){
				$admin_tariff = 0.075;
			}else{
				$admin_tariff = 0.068;
			}
		}else{
			$admin_tariff = $tariff;
		}
		$today_calls_a = $result_a['COUNT(`duration`)'];
		$today_calls_c = $result_c['COUNT(`duration`)'];
		$today_calls_d = $result_d['COUNT(`duration`)'];
		// if($today_calls < 500){
			$sumduration = $result_answered['SUM(`duration`)'];	
		// }else{
		// 	$sumduration = ($result_answered['SUM(`duration`)']+($result_answered['SUM(`duration`)']/100)*5);	
		// }

		$all_withdrawal = $result_all_withdrawal['SUM(`duration`)'];
		$time = secondsToTime($time->fetch_assoc()['SUM(`duration`)']);
		$today_time = secondsToTime($sumduration);
		$today_money = round(($sumduration/60)*$admin_tariff, 2).' $';
		$all_money = round(($all_withdrawal/60)*$admin_tariff, 2);
		$balance = ($user->getBalance()-$all_money).' $';
		if($this->security->isGranted('ROLE_ADMIN') && $clid == 0){
			$users_rep = $this->getDoctrine()->getRepository(User::class);
    		$closers = $users_rep->findByRole('ROLE_CLOSER');
    		$balance = 0;
    		foreach ($closers as $key => $value) {
    			$balance = $balance+$value->getBalance();
    		}
    		$balance = $balance-$all_money.' $';
		}
		mysqli_close($link);
        return $this->render('default/calls.html.twig', [
            'result' => $result,
            'timesum' => $time,
            'prevpage' => $pageno-1,
            'nextpage' => $pageno+1,
            'currpage' => $pageno,
            'tariff' => $tariff,
            'today_time' => $today_time,
            'today_money' => $today_money,
			'today_calls' => $today_calls_a,
			'today_calls_c' => $today_calls_c,
			'today_calls_d' => $today_calls_d,
			'dstparam' => $dstparam,
			'balance' => $balance,
			'opid' => $opid,
        ]);
    }
}
