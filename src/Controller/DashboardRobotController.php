<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Security\Core\Security;

use phpseclib\Net\SSH2;

use App\Form\DatabaseUploadType;
use App\Entity\Contacts;
use App\Entity\DatabaseCall;
use App\Entity\User;

use Velhron\DadataBundle\Service\DadataClean;

class DashboardRobotController extends AbstractController
{
	private $security;

	public function __construct(Security $security)
	{
		$this->security = $security;
	}

	/**
	 * @Route("/dashboard/robot/{status}", name="dashboard_robot")
	 */
	public function index(Request $request, SluggerInterface $slugger, DadataClean $dadataClean, $status = 1): Response
	{
		$user = $this->getUser();
		$user_id = $this->getUser()->getId();
		$entityManager = $this->getDoctrine()->getManager();

		$database = new DatabaseCall();

		$form = $this->createForm(DatabaseUploadType::class, $database);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$database->setName($form->get('name')->getData());
			$database->setType(2);
			$CSVFile = $form->get('file')->getData();
			if($CSVFile){
				$originalFilename = pathinfo($CSVFile->getClientOriginalName(), PATHINFO_FILENAME);
				$safeFilename = $slugger->slug($originalFilename);
				$newFilename = $safeFilename.'-'.uniqid().'.csv';
				 try {
					$CSVFile->move(
						$this->getParameter('OutboundUploadDir'),
						$newFilename
					);
				} catch (FileException $e) {
					// ... handle exception if something happens during file upload
				}
				$database->setFile("/uploads/OutBound/".$newFilename);
				$filePathCSV = $this->getParameter('OutboundUploadDir')."/".$newFilename;
				
				$database->setCloserId($user_id);
				$entityManager->persist($database);
				$entityManager->flush();
				$dbid = $database->getId();
			}
			
		}
		
		$entityManager = $this->getDoctrine()->getManager();
		$numbers = $entityManager->getRepository(Contacts::class)->findBy([
			'closer_id' => $user->getId(),
			'CallType' => 2,
		]);
		$numbers_all = count($numbers);
		$page = $_GET['pageno'] ?? 1;
		$onepagelimit = 10;
		$numbers = $entityManager->getRepository(Contacts::class)->findBy([
			'closer_id' => $user->getId(),
			'CallType' => 2,
		], ['id' => 'DESC'], $onepagelimit, ($page-1)*$onepagelimit);

		$pagedb = $_GET['pagenodb'] ?? 1;
		$dbs = $entityManager->getRepository(DatabaseCall::class)->findBy([
			'closer_id' => $user->getId(),
			'type' => 2,
		], ['id' => 'DESC'], $onepagelimit, ($pagedb-1)*$onepagelimit);

		if($status == 'success_start'){
			$show_success = 1;
		}elseif($status == 'error'){
			$show_success = 2;
		}else{
			$show_success = 0;
		}

		return $this->render('default/robot.html.twig', [
			'controller_name' => 'DashboardRobotController',
			'uploadForm' => $form->createView(),
			'numbers' => $numbers,
			'currpage' => $page,
			'prevpage' => $page-1,
			'nextpage' => $page+1,
			'dbs' => $dbs,
			'show_success' => $show_success
		]);
	}
}
