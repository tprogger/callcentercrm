<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use phpseclib\Net\SSH2;

use App\Form\DatabaseUploadType;
use App\Entity\Contacts;
use App\Entity\DatabaseCall;
use App\Entity\User;

use Velhron\DadataBundle\Service\DadataClean;
use League\Csv\Reader;

class DashboardRobotStartcallsController extends AbstractController
{
	private $security;

	public function __construct(Security $security)
	{
		$this->security = $security;
	}

    /**
     * @Route("/dashboard/robot/startcalls/{id}/{type}", name="dashboard_robot_startcalls_type")
     */
    public function index($id, $type, Request $request, SluggerInterface $slugger, DadataClean $dadataClean): Response
    {

    	// User Data
    	$user = $this->getUser();
    	$user_id = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();

        // Название EXT для Asterisk
        // Уникализируем вызовы робота
        $ext = 'ivr-outline-robot-'.$user_id.'-'.rand(111111111111,999999999999);

        //Подгружаем DB
		$database = $entityManager->getRepository(DatabaseCall::class)->findOneBy([
			'closer_id' => $user->getId(),
			'id' => $id,
		]);

		if($database){
			// Получаем имя файла для обработки
			$filename = $database->getFile();
			$filename = str_replace('/uploads/OutBound', '', $filename);

			// Конвертируем строку, если кодировка не верная
			function convert( $str ) {
				$enc = mb_detect_encoding($str);
				if($enc == 'ASCII'){
					return iconv( "Windows-1251", "UTF-8", $str );	
				}else{
					return $str;
				}
			}

			//Создаём SSH оединение до сервера телефонии
			$ssh = new SSH2($_ENV["MY_SERVER_IP"]);
			if (!$ssh->login($_ENV["MY_SERVER_LOGIN"], $_ENV["MY_SERVER_PASSWORD"])) {
				exit('Login Failed');
			}
			try {

				//Выбираем операторов для вызова
				$operators = $entityManager->getRepository(User::class)->findBy([
					'closer_id' => $user->getId(),
					'status' => 0
				]);
				$dialops = '';
				$i = 0;
				$len = count($operators);
		    	foreach($operators as $op) {
		    		if($len == 1){
		    			if($op->getStatus() < 1){
		    				$dialops .= 'SIP/'.$op->getSipId();
		    			}
		    		}else{
		    			if ($i == 0) {
			    			if($op->getStatus() < 1){
			    				$dialops .= 'SIP/'.$op->getSipId();
			    			}
					    }else{
					    	if ($i == $len - 1) {
					    		if($op->getStatus() < 1){
					    			$dialops .= '&SIP/'.$op->getSipId();
					    		}
					    	}else{
					    		if($op->getStatus() < 1){
					    			$dialops .= '&SIP/'.$op->getSipId();
					    		}
					    	}
					    }
					    $i++;
		    		}
				}

				// Выбираем спич файл
				$sound_file = '/var/lib/asterisk/sounds/stc-cloud_tts';

				// Создем EXT для Asterisk
				$extension = (
					'cat <<EOF >> /etc/asterisk/extensions_custom.conf'.PHP_EOL.PHP_EOL.
					'['.$ext.']'.PHP_EOL.
					'exten => s,1,Wait(1)'.PHP_EOL.
					'exten => s,n,Background('.$sound_file.')'.PHP_EOL.
					'exten => s,n,WaitExten(5)'.PHP_EOL.
					'exten => 1,1,Dial('.$dialops.')'.PHP_EOL.
					'exten => 2,1,Background(/var/lib/asterisk/sounds/balance)'.PHP_EOL.
					'exten => 2,2,Dial('.$dialops.')'.PHP_EOL.
					'exten => 0,1,HangUp()'.PHP_EOL.
					'EOF'.PHP_EOL.
					'asterisk -rx "sip reload"'.PHP_EOL.
					'asterisk -rx "dialplan reload"'.PHP_EOL
				);
				$ext_create = $ssh->exec($extension);

				//Читаем CSV
				$reader = Reader::createFromPath($this->getParameter('OutboundUploadDir').$filename, 'r');
				$reader->setDelimiter(';');
				$csv_data = $reader->getRecords(['fullname', 'phone', 'address', 'additional']);


				// Создаем и открываем архив
				$archive = new \ZipArchive();
				$archive_name = $this->getParameter('OutboundUploadDir').'/'.$user_id.'_'.$id.'_'.$type.'_'.rand(11111,99999).'.zip';
				$res = $archive->open($archive_name, \ZipArchive::CREATE);

				if ($res === TRUE) {
					// Обрабатываем CSV
					foreach ($csv_data as $key => $value) {
						$value = (object) $value;
						$phone = convert($value->phone);
						$fullname = convert($value->fullname);
						$address = convert($value->address);
						$additional = convert($value->additional);
						if(!is_null($phone) && !is_null($fullname)){
							if(is_null($additional)){
								$additional = 'Отсутствует';
							}
							if(is_null($address)){
								$address = 'Не был получен';
							}
							$phone = preg_replace('/[^0-9]/i', '', $phone);
							$phone_len = strlen($phone);
							if($phone_len == 10){
								$phone = '7'.$phone;
							}else{
								if($phone[0] == 8){
									$phone[0] = 7;
								}
							}

							//Записываем в БД
							$country = "Russia";
							$city = "Не известен";
							$region = "Не известен";
							$timezone = '+3';
							$contact = new Contacts();
							$contact->setWorkerId(0);
							$contact->setCloserId($user_id);
							$contact->setFullname($fullname);
							$contact->setDbId($id);
							$contact->setPhone($phone);
							$contact->setRegion($region);
							$contact->setCountry($country);
							$contact->setCity($address);
							$contact->setTimezone($timezone);
							$contact->setCallStatus(1);
							$contact->setCallTime(0);
							$contact->setCallType(2);
							$entityManager->persist($contact);
							$entityManager->flush();

							//Файл вызова
							$callfile_name = $user_id.'_'.$phone.'_'.rand(11111,99999).'.call';
							$callfile_data = (
								'Channel: SIP/'.$phone.'@voiptrade_out'.PHP_EOL.
								'MaxRetries: 1'.PHP_EOL.
								'RetryTime: 60'.PHP_EOL.
								'WaitTime: 30'.PHP_EOL.
								'Context: '.$ext.''.PHP_EOL.
								'Extension: s'.PHP_EOL.
								'Priority: 1'.PHP_EOL.
								'CallerID: '.$fullname.' <'.$phone.'>'.PHP_EOL
							);

							// Кидаем в архив
							$archive->addFromString($callfile_name, $callfile_data);
						}
					}
					$archive->close();
					// Генерируем ссылку для скачки
					$file_name = str_replace($this->getParameter('OutboundUploadDir').'/', '', $archive_name);
					$download_path = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['SERVER_NAME'].'/uploads/OutBound/'.$file_name;

					// Называем временную папку
					$temp_unzip_folder = '/var/spool/asterisk/temp/'.$type.'/'.$user_id.'/'.$id.'_'.rand(11111,99999);
					
					// Составляем команду
					$exec_command = 'mkdir -p "'.$temp_unzip_folder.'" && cd "'.$temp_unzip_folder.'" && wget "'.$download_path.'" && unzip "'.$file_name.'" && rm "'.$file_name.'" && mv ./* /var/spool/asterisk/outgoing/';

					// Выполняем Команду
					$exec = $ssh->exec($exec_command);

					// Удаляем архив
					unlink($archive_name);

					return $this->redirectToRoute('dashboard_robot', [
						'status' => 'success_start'
					]);

				} else {
				    return $this->redirectToRoute('dashboard_robot', [
						'status' => 'error'
					]);
				}
			}catch (Exception $e) {
			    return $this->redirectToRoute('dashboard_robot', [
					'status' => 'error'
				]);
			}
		}
    }
}
