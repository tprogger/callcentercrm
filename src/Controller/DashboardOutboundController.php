<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use phpseclib\Net\SSH2;

use App\Form\DatabaseUploadType;
use App\Entity\Contacts;
use App\Entity\DatabaseCall;
use App\Entity\User;

use Velhron\DadataBundle\Service\DadataClean;
use League\Csv\Reader;

class DashboardOutboundController extends AbstractController
{
	private $security;

	public function __construct(Security $security)
	{
		$this->security = $security;
	}

	/**
	 * @Route("/dashboard/outbound/{status}", name="dashboard_outbound")
	 */
	public function outbound(Request $request, SluggerInterface $slugger, DadataClean $dadataClean, $status = 1): Response
	{
		// Конвертируем строку, если кодировка не верная
		function convert( $str ) {
			$enc = mb_detect_encoding($str);
			if($enc == 'ASCII'){
				return iconv( "Windows-1251", "UTF-8", $str );	
			}else{
				return $str;
			}
		}

    	// User Data
    	$user = $this->getUser();
    	$user_id = $this->getUser()->getId();
        $entityManager = $this->getDoctrine()->getManager();
	
		$database = new DatabaseCall();
		$form = $this->createForm(DatabaseUploadType::class, $database);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$database->setName($form->get('name')->getData());
			$database->setType(1);
			$CSVFile = $form->get('file')->getData();
			if($CSVFile){
				$originalFilename = pathinfo($CSVFile->getClientOriginalName(), PATHINFO_FILENAME);
				$safeFilename = $slugger->slug($originalFilename);
				$newFilename = $safeFilename.'-'.uniqid().'.csv';
				try {
					$CSVFile->move(
						$this->getParameter('OutboundUploadDir'),
						$newFilename
					);
				} catch (FileException $e) {
					// ... handle exception if something happens during file upload
				}
				$database->setFile("/uploads/OutBound/".$newFilename);
				$filePathCSV = $this->getParameter('OutboundUploadDir')."/".$newFilename;
				$database->setCloserId($user_id);
				$entityManager->persist($database);
				$entityManager->flush();
				$dbid = $database->getId();
				try {
					//Читаем CSV
					$reader = Reader::createFromPath($filePathCSV, 'r');
					$reader->setDelimiter(';');
					$csv_data = $reader->getRecords(['fullname', 'phone', 'address', 'additional']);

					// Обрабатываем CSV
					foreach ($csv_data as $key => $value) {
						$value = (object) $value;
						$phone = convert($value->phone);
						$fullname = convert($value->fullname);
						$address = convert($value->address);
						$additional = convert($value->additional);
						if(!is_null($phone) && !is_null($fullname)){
							
							if(is_null($additional)){
								$additional = 'Отсутствует';
							}
							if(is_null($address)){
								$address = 'Не был получен';
							}

							$phone = preg_replace('/[^0-9]/i', '', $phone);
							$phone_len = strlen($phone);
							if($phone_len == 10){
								$phone = '7'.$phone;
							}else{
								if($phone[0] == 8){
									$phone[0] = 7;
								}
							}

							//Записываем в БД
							$country = "Russia";
							$city = "Не известен";
							$region = "Не известен";
							$timezone = '+3';
							$contact = new Contacts();
							$contact->setWorkerId(0);
							$contact->setCloserId($user_id);
							$contact->setFullname($fullname);
							$contact->setDbId($dbid);
							$contact->setPhone($phone);
							$contact->setRegion($region);
							$contact->setCountry($country);
							$contact->setCity($address);
							$contact->setTimezone($timezone);
							$contact->setCallStatus(0);
							$contact->setCallTime(0);
							$contact->setCallType(1);
							$entityManager->persist($contact);
							$entityManager->flush();
						}
					}
					return $this->redirectToRoute('dashboard_outbound', [
						'status' => 'success_upload'
					]);
				}catch (Exception $e) {
				    return $this->redirectToRoute('dashboard_outbound', [
						'status' => 'error'
					]);
				}
			}
		}

		$numbers = $entityManager->getRepository(Contacts::class)->findBy([
			'closer_id' => $user_id,
			'CallType' => 1,
		]);
		$numbers_all = count($numbers);
		$page = $_GET['pageno'] ?? 1;
		$onepagelimit = 10;
		$numbers = $entityManager->getRepository(Contacts::class)->findBy([
			'closer_id' => $user_id,
			'CallType' => 1,
		], ['id' => 'DESC'], $onepagelimit, ($page-1)*$onepagelimit);

		$pagedb = $_GET['pagenodb'] ?? 1;
		$dbs = $entityManager->getRepository(DatabaseCall::class)->findBy([
			'closer_id' => $user_id,
			'type' => 1,
		], ['id' => 'DESC'], $onepagelimit, ($pagedb-1)*$onepagelimit);

		if($status == 'success_upload'){
			$show_success = 1;
		}elseif($status == 'error'){
			$show_success = 2;
		}else{
			$show_success = 0;
		}

		return $this->render('default/outbound.html.twig', [
			'uploadForm' => $form->createView(),
			'numbers' => $numbers,
			'currpage' => $page,
			'prevpage' => $page-1,
			'nextpage' => $page+1,
			'dbs' => $dbs,
			'show_success' => $show_success,
		]);
    }
}
