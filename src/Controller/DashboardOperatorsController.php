<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

use App\Entity\User;
ini_set('memory_limit', '-1');
class DashboardOperatorsController extends AbstractController
{
	private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/dashboard/operators", name="dashboard_operators")
     */
    public function index()
    {
        if (isset($_GET['pageno'])) {
		    $pageno = $_GET['pageno'];
		} else {
		    $pageno = 1;
		}
		$no_of_records_per_page = 10;
        $offset = ($pageno-1) * $no_of_records_per_page;

    	$user = $this->getUser();
    	$user_role = $user->getRoles();
    	if($this->security->isGranted('ROLE_ADMIN')){
    		$operators = $this->getDoctrine()->getRepository(User::class)->findBy([
	    		
            ], ['id' => 'DESC'], 10, $offset);
            $alloperators = $this->getDoctrine()->getRepository(User::class)->findBy([
	    		
            ], ['id' => 'DESC']);
    	}else{
    		$operators = $this->getDoctrine()->getRepository(User::class)->findBy([
	    		'closer_id' => $user->getId()
            ], ['id' => 'DESC'], 10, $offset);
            array_push($operators, $user);
            $alloperators = $this->getDoctrine()->getRepository(User::class)->findBy([
	    		'closer_id' => $user->getId()
            ], ['id' => 'DESC']);
            array_push($alloperators, $user);
    	}

        $whereclause = '';
        $i = 0;

        $len = count($operators);
        foreach($alloperators as $op) {
            if ($i == 0) {
               $whereclause .= 'cnum = \''.$op->getSipId().'\'';
            }else{
                if ($i == $len - 1) {
                    $whereclause .= ' OR cnum = \''.$op->getSipId().'\'';
                }else{
                    $whereclause .= ' OR cnum = \''.$op->getSipId().'\'';
                }
            }
            $i++;
        }

        
        
        $link = mysqli_connect($_ENV['MY_MYSQL_HOST'], $_ENV['MY_MYSQL_USER'], $_ENV['MY_MYSQL_PASSWORD'], $_ENV['MY_MYSQL_ASTERISK_DB']);

        if (!$link) {
            echo "Error: Unable to connect to MySQL." . PHP_EOL;
            echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }

        $queryy = "SELECT SQL_CACHE * FROM `cdr` WHERE ".$whereclause." ORDER BY `calldate` DESC LIMIT $offset,$no_of_records_per_page";
        $counts = $link->query($queryy)->num_rows;

        $queryy_all = "SELECT SQL_CACHE * FROM `cdr` WHERE ".$whereclause." ORDER BY `calldate` DESC";
        $counts_all = $link->query($queryy_all)->num_rows;

        $querys = "SELECT SQL_CACHE COUNT(`disposition`) FROM `cdr` WHERE ".$whereclause." AND disposition = 'ANSWERED'";
        // echo $querys;
        $counts_answered = $link->query($querys)->fetch_assoc()["COUNT(`disposition`)"];
        // var_dump($counts_answered);


        return $this->render('default/operators.html.twig', [
            'operators' => $operators,
            'calls' => $counts_all,
            'operators_cnt' => count($alloperators),
            'answered_calls' => $counts_answered,
            'nextpage' => $pageno+1,
            'prevpage' => $pageno-1,
            'currpage' => $pageno,
        ]);
    }

    /**
     * @Route("/dashboard/operators/{id}/ban", name="dashboard_ban")
     */
    public function ban($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $operator = $entityManager->getRepository(User::class)->findOneBy(['id' => $id]);
        $operator->setStatus(3);
        $entityManager->flush();
        return $this->redirectToRoute('dashboard_operators');
    }

    /**
     * @Route("/dashboard/operators/{id}/start", name="dashboard_start")
     */
    public function start($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $operator = $entityManager->getRepository(User::class)->findOneBy(['id' => $id]);
        $operator->setStatus(0);
        $entityManager->flush();
        return $this->redirectToRoute('dashboard_operators');
    }

    /**
     * @Route("/dashboard/operators/{id}/pause", name="dashboard_pause")
     */
    public function pause($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $operator = $entityManager->getRepository(User::class)->findOneBy(['id' => $id]);
        $operator->setStatus(1);
        $entityManager->flush();
        return $this->redirectToRoute('dashboard_operators');
    }
}
