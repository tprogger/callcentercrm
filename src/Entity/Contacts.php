<?php

namespace App\Entity;

use App\Repository\ContactsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContactsRepository::class)
 */
class Contacts
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $worker_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $closer_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fullname;

    /**
     * @ORM\Column(type="integer")
     */
    private $db_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $region;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="integer")
     */
    private $timezone;

    /**
     * @ORM\Column(type="integer")
     */
    private $call_status;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $call_time;

    /**
     * @ORM\Column(type="integer")
     */
    private $CallType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWorkerId(): ?int
    {
        return $this->worker_id;
    }

    public function setWorkerId(int $worker_id): self
    {
        $this->worker_id = $worker_id;

        return $this;
    }

    public function getCloserId(): ?int
    {
        return $this->closer_id;
    }

    public function setCloserId(int $closer_id): self
    {
        $this->closer_id = $closer_id;

        return $this;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(?string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function getDbId(): ?int
    {
        return $this->db_id;
    }

    public function setDbId(int $db_id): self
    {
        $this->db_id = $db_id;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getTimezone(): ?int
    {
        return $this->timezone;
    }

    public function setTimezone(int $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    public function getCallStatus(): ?int
    {
        return $this->call_status;
    }

    public function setCallStatus(int $call_status): self
    {
        $this->call_status = $call_status;

        return $this;
    }

    public function getCallTime(): ?string
    {
        return $this->call_time;
    }

    public function setCallTime(string $call_time): self
    {
        $this->call_time = $call_time;

        return $this;
    }

    public function getCallType(): ?int
    {
        return $this->CallType;
    }

    public function setCallType(int $CallType): self
    {
        $this->CallType = $CallType;

        return $this;
    }
}
