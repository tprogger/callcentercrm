<?php

namespace App\Repository;

use App\Entity\DatabaseCall;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DatabaseCall|null find($id, $lockMode = null, $lockVersion = null)
 * @method DatabaseCall|null findOneBy(array $criteria, array $orderBy = null)
 * @method DatabaseCall[]    findAll()
 * @method DatabaseCall[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DatabaseCallRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DatabaseCall::class);
    }

    // /**
    //  * @return DatabaseCall[] Returns an array of DatabaseCall objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DatabaseCall
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
